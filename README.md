## Berthor

This project is a simple domain names list using words defined under the `/data` directory.

### Installing

You can clone te repo with:

```
git clone https://gitlab.com/iorrah/berthor.git
```

### Starting up

You just need to open the `index.html` file to get started.

### Built with

- HTML
- CSS
- JavaScript

### Credits

A lot of inspiration has been taken from [fantasynamegenerators.com](https://www.fantasynamegenerators.com) so big thanks for the project!
